# -*- coding: utf-8 -*-
{
    'name': "Testing Module",

    'summary': """
        Testing module which add new field in customer view.""",

    'description': """
        Module will add new field in customer.
    """,

    'author': "Krupesh Laiya",
    # krupesh.laiya@gmail.com
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Testing',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'contacts'],

    # always loaded
    'data': [
        #'security/ir.model.access.csv',
        'views/res_partner_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
    #    'demo/demo.xml',
    ],
}
