# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ResPartner(models.Model):
    _inherit = 'res.partner'

    custom_test = fields.Char("Custom Field")


ResPartner()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
